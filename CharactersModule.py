class Player:
    def __init__(self, name, sub_class, unique_ability):
        self.name = name
        self.sub_class = sub_class
        self.unique_ability = unique_ability
        self.level = 1
        self.health = 100
        self.damage = 5
        self.experience = 0
        self.mobs_count = 0

    def __repr__(self):
        description = f'Player {self.name} is a {self.level} lvl {self.sub_class}, have {self.health} HP and unique ability "{self.unique_ability}" with {self.damage} damage from it.'
        description += f" His experience equal to {self.experience}."
        return description
    
    def lvl_up(self):
        if self.experience >= 4 and self.experience <= 10:
            self.level = 2
            self.damage = 7
            self.health = 110
        elif self.experience >= 11 and self.experience <= 20:
            self.level = 3
            self.damage = 10
            self.health = 130
        print(f"You riched lvl {self.level}, your HP equal to {self.health}")

    def take_damage(self, damage_count):
        if damage_count >= self.health:
            print("Game over!")
        else:
            self.health = self.health - damage_count
            print(f"{self.name} HP equal to {self.health}")
    
    def increase_experience(self, mob):
        self.experience += mob.exp_cost

    def make_damage(self, mob):
        print(f"{self.name} make {self.damage} points damage to {mob.name}.")
        if mob.take_damage(self.damage):
            self.increase_experience(mob)
            self.mobs_count += 1
            if self.experience >= 4:
                self.lvl_up()



class Mob:
    def __init__(self, name, sub_class):
        self.name = name
        self.sub_class = sub_class
        self.level = 1
        self.damage = 1
        self.health = 5
        self.exp_cost = 2

    def __repr__(self):
        description = f"Mob {self.name} is a {self.level} lvl {self.sub_class}, have {self.health} HP."
        return description

    def lvl_up(self):
        self.level += 1
        self.health += 5
        print(f"Mob lvl incrised to {self.level}, HP incrised to {self.health}")

    def take_damage(self, damage_count):
        if damage_count >= self.health:
            print("Successfull! Monster died!")
            self.health = 0
            return True
        else:
            self.health = self.health - damage_count
            print(f"{self.name} remains {self.health} HP. Make next punch to kill it!")
            return False
           
