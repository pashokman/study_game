from CharactersModule import Player, Mob

#create player and mob
player1 = Player("Destroyer2000", "Warior", "Powerful Punch")
print(player1)

mob1 = Mob("Orc", "Shaman")
print(mob1)

#player1 punch a mob1
print()
print("--------------------------------")

player1.make_damage(mob1)
print(player1)
print(mob1)

#player1 punch a mob2
print()
print("--------------------------------")
mob2 = Mob("Elf", "Dagger")

player1.make_damage(mob2)
print(player1)
print(mob2)

#player1 punch a mob3
print()
print("--------------------------------")
mob3 = Mob("Gnom", "Spoiler")

player1.make_damage(mob3)
print(player1)
print(mob3)

#player1 punch a mob4
print()
print("--------------------------------")
mob4 = Mob("Gnom", "Spoiler")

player1.make_damage(mob4)
print(player1)
print(mob4)

#player1 punch a mob5
print()
print("--------------------------------")
mob5 = Mob("Gnom", "Spoiler")

player1.make_damage(mob5)
print(player1)
print(mob5)
#player1 punch a mob6
print()
print("--------------------------------")
mob6 = Mob("Gnom", "Spoiler")

player1.make_damage(mob6)
print(player1)
print(mob6)


